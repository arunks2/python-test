import mysql.connector

class Connector:

    def __init__(self, user, password, host, database):
        self.user = user
        self.password = password
        self.host = host
        self.database = database

    def connect(self):
        config = {
            'user' : self.user,
            'password' : self.password,
            'host' : self.host,
            'database' : self.database
        }
        return mysql.connector.connect(**config)

    def cursor(self):
        cnx = self.connect()
        return cnx.cursor(buffered=True)